<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// $router->post('/register', 'AuthController@register');
// $router->post('/login', 'AuthController@login');
// $router->get("/user", "UserController@index");
// $router->get('/api/v1//users/logout', 'UserController@logout');

$router->group(['prefix' => 'api'], function () use($router){

    $router->group(['prefix'=>'v1'], function() use($router){

        // $router->group(['//'], function() use($router){

            $router->group(['prefix'=>'users'], function() use($router){
                $router->post('/login', 'AuthController@login');
                $router->get('/logout', 'UserController@logout');
            });

            $router->group(['prefix'=>'topic'], function() use($router){
                $router->post('/create', 'TopicController@create');
                $router->post('/update', 'TopicController@update');
                $router->get('/list', 'TopicController@list');
                $router->get('/{topic}', 'ArticleController@getArticleTopic');
            });
        // });

        $router->group(['prefix'=>'article'], function() use($router){
            $router->post('/create', 'ArticleController@create');
            $router->post('/update', 'ArticleController@update');
            $router->get('/list', 'ArticleController@list');
            $router->get('/{slug}', 'ArticleController@getArticleSingle');
        });

    });

});