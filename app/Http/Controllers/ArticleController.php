<?php

namespace App\Http\Controllers;

use App\Article;
use App\Topic;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware("login");
    }

    // public function index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function create(Request $request){
        $topic_id= $request->input('topic_id');
        $title = $request->input('title');
        $body = $request->input('body');
        $slug = $request->input('slug');

        //find topic id
        $topic_from_id_topic = Topic::where('id', $topic_id)->first();
        if($topic_from_id_topic){
            $create_article = Article::create([
                'topic_id' => $topic_id,
                'title' => $title,
                'body' => $body,
                'slug' => $slug
            ]);

            if($create_article){
                return response()->json([
                    "status:"=> "success created",
                    "topic id: "=> $topic_id,
                    "title" => $title,
                    "slug" => $slug,
                    "body" => $body
                ], 201);
            }
            else{
                return response()->json([
                    "status:"=> "Failed creating"
                ], 400);
            }
        }
        else{
            return response()->json([
                "status:"=> "Cannot find topic"
            ], 400);
        }
    }

    public function update(Request $request){
        $id= $request->input('topic_id');

        //find topic id
        // $topic_from_id_topic = Topic::where('id', $id[0])->first();
        $article = Article::where('topic_id', $id[0])->first();
        if($article){
            $article->update([
                'topic_id' => $id[1]
            ]);

            return response()->json([
                    "status:"=> "success updated",
                    "topic id: "=> $article
                ], 201);

        }
        else{
            return response()->json([
                "status:"=> "Cannot find article"
            ], 400);
        }
    }

    public function list(Request $request){
        $article = Article::all();
        if($article){
            return response()->json([
                "status:"=> "success",
                "articles:"=> $article
            ], 201);
        }
        else{
            return 'gak ada artikel';
        }
    }

    public function getArticleTopic($topic){
        $topic = Topic::where('slug',$topic)->first();
        $article = Article::where('topic_id', $topic->id)->first();

        if($article){
            return response()->json([
                "status:"=> "success",
                "articles:"=> $article
            ], 201);
        }
        else{
            return 'gak ada artikel';
        }
    }

    public function getArticleSingle($article){
       $article = Article::where('slug', $article)->first();

        if($article){
            return response()->json([
                "status:"=> "success",
                "articles:"=> $article
            ], 201);
        }
        else{
            return 'gak ada artikel';
        }
    }
}