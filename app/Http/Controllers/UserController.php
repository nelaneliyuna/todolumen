<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware("login");
    }

    public function index()
    {
        return "Anda Berhasil masuk";
    }

    public function logout(Request $request){
        // return "Anda belum logout";
        $user =  User::where('token', $request->header('X-Access-Token'))->first();
        // echo($user);


        if($user){
            //kalau bener ntar generate token & disave di db
           $user->update([
                'token' => ''
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Logged out, bye!'
            ], 201);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => 'Wrong token / not logged in'
            ], 400);
        }
    }
}