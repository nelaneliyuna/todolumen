<?php

namespace App\Http\Controllers;

use App\Topic;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class TopicController extends Controller
{

    public function __construct()
    {
        $this->middleware("login");
    }

    // public function index()
    // {
    //     return "Anda Berhasil masuk";
    // }

    public function create(Request $request){
        $name= $request->input('topic_name');
        $slug= $request->input('slug');

        $create_topic = Topic::create([
            'name' => $name,
            'slug' => $slug
        ]);

        if($create_topic){
            return response()->json([
                "status:"=> "success created",
                "topic: "=> $name
            ], 201);
        }
        else{
            return response()->json([
                "status:"=> "Failed creating",
                "topic:"=>  $name
            ], 400);
        }
    }

    public function update(Request $request){
        $id= $request->input('topic_id');
        $name= $request->input('topic_name');

        //find topics for updating
        $topic = Topic::where('id', $id)->first();

        if($topic){
            $topic->update([
                'name' => $name
            ]);

            return response()->json([
                "status:"=> "success updated",
                "new topic: "=> $name
            ], 201);
        }
        else{
            return response()->json([
                "status:"=> "Fail updating topic",
                "id:"=> $id
            ], 400);
        }
    }

    public function list(Request $request){
        $topic = Topic::all();
        if($topic){
            return response()->json([
                "status:"=> "success",
                "topics:"=> $topic
            ], 201);
        }
        else{
            return 'gak ada topik :(';
        }
    }
}