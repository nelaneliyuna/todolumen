<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     //
    // }

    // //

    // public function register(Request $request){
    //     $email = $request->input('email');
    //     $password = Hash::make($request->input('password'));

    //     $register = User::create([
    //         'email' => $email,
    //         'password' => $password
    //     ]);

    //     if($register){
    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Register success',
    //             'data' => $register
    //         ], 201);
    //     }
    //     else{
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Register fail',
    //             'data' => ''
    //         ], 400);
    //     }
    // }

    public function login(Request $request){
       //set variable of email and password
       $email = $request->input('email') ;
       $password = $request->input('password');

       //find user
       $user = User::where('email', $email)->first();
    //    echo($user);

    //    //hashed pw
    //    $hashed_pw = Hash::make($user->password);
    //    echo($hashed_pw);

    //    $hashed_pw_input = Hash::make($password);
    //    echo($password);
    //cek ada user gak
        if($user){
            //kalau bener ntar generate token & disave di db
            if($password == $user->password){
                $token = 'asdfasdfasdfasdfasdfasdf';

                $user->update([
                    'token' => $token
                ]);

                return response()->json([
                    'status' => true,
                    'message' => 'Login Success yay!',
                    'token' => $token
                ], 201);
            }
            else{
                return response()->json([
                    'status' => false,
                    'message' => 'Wrong password'
                ], 400);
            }
        }
        else{
            return response()->json([
                'status' => false,
                'message' => 'User not found'
            ], 400);
        }
    }
}

