<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // echo($request->header('X-Access-Token'));
        if (($request->header('X-Access-Token'))) {
            $check =  User::where('token', $request->header('X-Access-Token'))->first();

            if (!$check) {
                return response('Anda Belum Login atau Token Tidak Valid.', 401);
            } else {
                return $next($request);
            }
        } else {
            return response('Silahkan Login dan Masukkan Token.', 401);
        }
    }
}
