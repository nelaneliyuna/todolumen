Hello, this is my first Lumen Project.

I use Postgres for the Database.

To run this project in your local, you can remote this project.

If you have postgresql in your local, please run `psql -U postgres --host localhost` and then enter your password.

Create new database using `CREATE DATABASE lumendb;`

In the directory of project, please run `composer install`.

Please add .env file in your directory, and add this configuration:
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=
APP_TIMEZONE=UTC

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=lumendb
DB_USERNAME=postgres
DB_PASSWORD=password

CACHE_DRIVER=file
QUEUE_DRIVER=sync
```

You can run the project in terminal using: ` php -S localhost:8080 -t public`
And run the migration of database using: ` php artisan migrate`

Thank you, have fun :)


